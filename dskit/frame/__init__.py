from .frame import Dummifier
from .frame import Encoder
from .frame import InverseEncoder

from .frame import dummifier
from .frame import encoder
