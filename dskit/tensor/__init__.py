from .tensor import batch
from .tensor import batches
from .tensor import cycle
from .tensor import gridrange
from .tensor import iteraxis
from .tensor import move
from .tensor import slices
